import numpy as np
from scipy.special import gamma as gamma_func
import mpmath

@np.vectorize
def hyp1f2(a1, b1, b2, z):
    return float(mpmath.hyp1f2(a1, b1, b2, z))

from ._bounded_kernel_base import _BoundedKernelBase

class Cosine(_BoundedKernelBase):
    def _pdf_noscale(self, x):
        return (np.pi/4)*np.cos(np.pi*x/2)
    
    def _sample_noscale(self, size, rng):
        tmp = rng.uniform(low=-1, high=+1, size=size)
        return 2*np.arcsin(tmp)/np.pi
    
    def _absolute_moment_noscale(self, k):
        a1 = (k+1)/2
        b1, b2 = 1/2, (k+3)/2
        z = -np.pi**2/16
        return np.pi * hyp1f2(a1, b1, b2, z) / (2*k+2)
    
    _allowed_absolute_moments_str = "Only k > -1 is allowed."
    def _absolute_moment_exists(self, k):
        return (k > -1)

class GeneralizedTriangle(_BoundedKernelBase): # Find actual name
    def __init__(self, n):
        assert n > -1
        self.n = n
    
    def _pdf_noscale(self, x):
        return (self.n+1) * (1-np.absolute(x))**self.n / 2
    
    def _sample_noscale(self, size, rng):
        u = rng.random(size)
        x = 1 - (1-u)**(1/(self.n+1))
        return x*rng.choice([-1, 1], size=size)
    
    def _absolute_moment_noscale(self, k):
        nr = gamma_func(k+1)*gamma_func(self.n+2)
        dr = gamma_func(k+self.n+2)
        return nr/dr
    
    _allowed_absolute_moments_str = "Only k > -1 is allowed."
    def _absolute_moment_exists(self, k):
        return (k > -1)

# class Generalized(_KernelBase):
#     # p(x) ~ (1-|x|^m)^n
#     def __init__(self, n):
#         assert n > -1
#         self.n = n
    
#     def _pdf_noscale(self, x):
#         return 