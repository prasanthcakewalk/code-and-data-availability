import numpy as np
import numbers

class _BoundedKernelBase:
    def pdf(self, x, scale=1.):
        # Perform some scale checks
        
        is_array = True
        is_0d = False
        
        if not isinstance(x, np.ndarray):
            is_array = False
        
        try:
            x = np.array(x, copy=True)
        except:
            raise(ValueError, "x should be a float or a real-valued array-like object.")
        
        if x.shape == ():
            is_0d = True
        
        x = np.atleast_1d(x)
        
        # if not np.all(np.isreal(x)):
        if not np.isrealobj(x):
            raise(ValueError, "x should be a float or a real-valued array-like object.")
        
        ####################################################
        
        x /= scale
        
        mask = (-1 <= x) & (x <= 1)
        x = np.where(mask, x, 0) # use 0 as a placeholder for x outside the support
        ans = np.where(mask, self._pdf_noscale(x), 0) / scale
        
        if is_0d and (not is_array):
            ans = ans[0]
        
        return ans
    
    def sample(self, scale=1., size=1, random_state=None):
        # Perform some checks and input sanitization
        
        if random_state is None or random_state is np.random:
            rng = np.random.RandomState()
        elif isinstance(random_state, numbers.Integral):
            rng = np.random.RandomState(seed=random_state)
        elif isinstance(random_state, (np.random.RandomState, np.random.Generator)):
            rng = random_state
        else:
            raise ValueError(f'Invalid random_state value provided.')
        
        return scale*self._sample_noscale(size, rng)
    
    def absolute_moment(self, k, scale=1.):
        return self._absolute_moment_noscale(k) * scale**k
    
    def _pdf_noscale(self, x):
        raise NotImplementedError
    
    def _sample_noscale(self, size, rng):
        raise NotImplementedError
    
    def _absolute_moment_noscale(self, k):
        raise NotImplementedError
    
    _allowed_absolute_moments_str = "Allowed values of k is not implemented."
    def _absolute_moment_exists(self, k):
        raise NotImplementedError