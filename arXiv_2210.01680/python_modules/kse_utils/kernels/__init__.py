__all__ = [
    'Delta', 'Rectangular', 'Triangular',
    'Cosine', 'GeneralizedTriangle',
    'SymmetricBeta',
    'WignerNSphere',
    'Arcsine',
    'WignerSemicircle',
    'Epanechnikov', 'Parabolic',
    'Quartic', 'BiWeight',
    'TriWeight', 'ArcSine',
]

from ._simple_kernels import Delta, Rectangular, Triangular
from ._advanced_kernels import Cosine, GeneralizedTriangle

from ._symmetric_beta_kernels import (
    SymmetricBeta, # alpha = beta
    WignerNSphere, # alpha = beta = (n+1)/2
	ArcSine, # alpha = beta = 1/2
	WignerSemicircle, # alpha = beta = 3/2
    Epanechnikov, Parabolic, # alpha = beta = 2
    Quartic, BiWeight, # alpha = beta = 3
    TriWeight, # alpha = beta = 4
)