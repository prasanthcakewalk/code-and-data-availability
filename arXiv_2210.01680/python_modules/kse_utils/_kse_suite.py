import numpy as np
from .kernels._bounded_kernel_base import _BoundedKernelBase

class KSESuite:
    def __init__(self, kernel, param_shape, pow_self=1., pow_others=0.):
        if not isinstance(kernel, _BoundedKernelBase):
            raise(ValueError, '`kernel` should be a valid kernel instance.')
        
        self.kernel = kernel
        self.param_shape = param_shape
        self.pow_self = pow_self
        self.pow_others = pow_others
        
        param_dim = sum(self.param_shape)
        self._regression_target_dr = (
            self.kernel.absolute_moment(self.pow_self, scale=1.0) *
            self.kernel.absolute_moment(self.pow_others, scale=1.0)**(param_dim-1)
        )
    
    def kse_type1_gen(self, N, scale=1., param=0.,
                      random_state=None):
        size = (N,) + self.param_shape
        scale = np.array(scale, copy=True)
        param = np.array(param, copy=True)
        
        u = self.kernel.sample(scale=1.0, size=size, random_state=random_state)
        
        tmp = np.prod(
            np.absolute(u)**self.pow_others,
            axis=tuple(range(1, len(self.param_shape)+1)),
            keepdims=True
        )
        regression_target_nr = tmp*np.sign(u)*np.absolute(u)**(self.pow_self-self.pow_others)
                
        paramprime = param + u*scale
        regression_target = regression_target_nr/(self._regression_target_dr*scale)
        
        return paramprime, regression_target
    
    # def kse_type1_gen(self, scale, N,
    #                   pow_self=1., pow_others=0., pow_all=None,
    #                   random_state=None):
    #     # scale should be broadcastable to shape (N,) + self.param_shape.
    #     # pow_self and pow_others should be floats or 1d-arrays broadcastable to size N.
    #     # pow_all should be array-like and brooadcastable to shape (N,) + self.param_shape + self.param_shape.
    #     # psi(x)_i = sign(x_i) * |x_i|^pow_all[i][i] * prod_j |x_j|^pow_all[i][j]
        
    #     size = (N,) + self.param_shape
    #     eps = self.kernel.sample(scale, size, random_state)
        
    #     if pow_all is not None:
    #         pow = pow_all
        
    #     else:
    #         pow_shape = (N,) + self.param_shape + self.param_shape
    #         pow = np.zeros(shape=pow_shape)
    #         pow = pow_others[:,np.newaxis,np.newaxis]
            
    #         for idx in np.ndindex(self.param_shape):
    #             pow[:][idx][idx] = pow_self[:]
        
    #     regression_target_nr = np.sign(eps)
    #     regession_target_dr = np.ones_like(eps)
        
    #     for idx_i in np.ndindex(self.param_shape):
    #         for idx_j in np.ndindex(self.param_shape):
    #             regression_target_nr[:][idx_i] *= np.absolute(eps)**pow[:][idx_i][idx_j]
    #             regession_target_dr *= self.kernel.absolute_moment(pow[:][idx_i][idx_j]+1)