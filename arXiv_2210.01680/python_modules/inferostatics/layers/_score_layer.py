import numpy as np
import tensorflow as tf
from ._base_layer import _ScoreLLRBase

class ScoreLayer(_ScoreLLRBase):
    def __init__(self, potential_fn, expand_fn_args=False,
                 output_slice=None, param_shape=None, output_mask=None,
                 return_potential_on_call=False):
        """_summary_

        Args:
            potential_fn (_type_): _description_
            expand_fn_args (bool, optional): _description_. Defaults to False.
            output_slice (_type_, optional): _description_ [[d0_start, d0_end, d0_stride], [d1_start, d1_end, d1_stride], ..., ]. Defaults to None.
            param_shape (_type_, optional): _description_. Defaults to None.
            output_mask (_type_, optional): _description_. Defaults to None.

        Raises:
            ValueError: _description_
        """
        super().__init__(potential_fn, expand_fn_args)
        
        # To avoid clashes with internal attribute and method names #######
        tmp = dir(self)
        for name in ['slice_spec', 'score_output_mask',
                     'return_potential_on_call']:
            assert name not in tmp
        ###################################################################
        
        if None not in [output_slice, output_mask]:
            raise ValueError("Only one of `output_slice` and `output_mask` can be non-None.")
        
        if (output_slice is not None) and (param_shape is None):
            raise ValueError("`param_shape` cannot be None when using the `output_slice` option.")
        
        if output_slice is not None:
            # Build slice object ################################
            self.slice_spec = tuple(
                [slice(*dim_spec) for dim_spec in output_slice]
            )
            #####################################################
            
            # Construct output_mask #############################
            self.score_output_mask = np.full(param_shape, fill_value=False)
            self.score_output_mask[self.slice_spec] = True
            #####################################################
            
            # Extend slice_spec to incorporate batch index ######
            self.slice_spec = (slice(None, None, None), ) + self.slice_spec
            #####################################################
        elif output_mask is not None:
            self.slice_spec = None
            self.score_output_mask = np.array(
                output_mask, dtype=bool, copy=True
            )
        else:
            self.slice_spec = None
            self.score_output_mask = None
        
        self.return_potential_on_call = return_potential_on_call
    
    def call(self, x, param):
        with tf.GradientTape() as tape:
            tape.watch(param)
            
            if self.score_output_mask is not None:
                masked_param = param*self.score_output_mask + \
                    tf.stop_gradient(param*np.invert(self.score_output_mask))
                potential = self.potential_callable([x, masked_param])
            else:
                potential = self.potential_callable([x, param])
                
            score = tape.gradient(potential, param)
        
        if self.slice_spec is not None:
            grad = score[self.slice_spec]
        else:
            grad = score
        
        if self.return_potential_on_call:
            return grad, potential
        else:
            return grad