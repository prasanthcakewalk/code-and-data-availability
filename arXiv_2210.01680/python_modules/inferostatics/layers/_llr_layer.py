import tensorflow as tf
from ._base_layer import _ScoreLLRBase

class LLRLayer(_ScoreLLRBase):
    def __init__(self, potential_fn, expand_fn_args=False,
                 output_mode='categorical_logit', output_for='both'):
        super().__init__(potential_fn, expand_fn_args)
        
        # To avoid clashes with internal attribute and method names #######
        tmp = dir(self)
        for name in ['output_mode', 'output_for']:
            assert name not in tmp
        ###################################################################
        
        self.output_mode, self.output_for = self._process_output_args(
            output_mode, output_for
        )
    
    @staticmethod
    def _process_output_args(output_mode, output_for,
                             output_mode_name='output_mode',
                             output_for_name='output_for'):
        output_mode_options = {
            'logit': '[log(p0/p1), log(p1/p0)]',
            'prob': '[p0/(p0+p1), p1/(p0+p1)]',
            'logprob': '[log(p0/(p0+p1)), log(p1/(p0+p1))]',
            'ratio': '[p0/p1, p1/p0]',
            'categorical_logit': '[log(p0/Z), log(p1/Z)] (Z is arbitrary)'
        }
        
        if output_mode not in output_mode_options:
            raise ValueError(
                f"`{output_mode_name}` must be one of the following (descriptions are for `output_for` = 'both'):\n" + 
                '\n'.join([
                    f'    {key}: {value}'
                    for key, value in output_mode_options.items()
                ])
            )
        
        if output_for not in ['0', '1', 'both']:
            raise ValueError(
                f"`{output_for_name}` must be '0', '1', or 'both' (strings).\n"
                "The respective output shapes are (batch, 1), (batch, 1), and (batch, 2).\n"
                "They correspond to log(p0/p1), log(p1/p0), and [log(p0/p1), log(p1/p0)], respectively (assuming `output_mode`='logit').\n"
                "Only `output_for`='both' is compatible with `output_mode`='categorical_logit'."
            )
        
        if output_mode == 'categorical_logit' and output_for != 'both':
            raise ValueError(
                f"`{output_for_name}` must be 'both' if `{output_mode_name}` = 'categorical_logit'."
            )
        
        if output_for in ['0', '1']:
            output_for = int(output_for)
        else:
            output_for = 'both'
        
        return output_mode, output_for
    
    def call(self, x, param_0, param_1):
        categorical_logit = [
            self.potential_callable([x, param_0]),
            self.potential_callable([x, param_1])
        ]
        
        return self._call_from_categorical_logit(
            categorical_logit, self.output_mode, self.output_for
        )
    
    @staticmethod
    def _call_from_categorical_logit(categorical_logit,
                                     output_mode, output_for):
        output_transform = {
            'categorical_logit': {
                'logit': lambda cl: cl - tf.reverse(cl, axis=-1),
                'prob': lambda cl: tf.nn.softmax(cl, axis=-1), 
                'logprob': lambda cl: tf.nn.log_softmax(cl, axis=-1),
                'ratio': lambda cl: tf.math.exp(cl - tf.reverse(cl, axis=-1)),
                'categorical_logit': lambda cl: cl,
            },
            'logit': {
                'logit': lambda logit: logit,
                'prob': tf.math.sigmoid,
                'logprob': tf.math.log_sigmoid,
                'ratio': tf.math.exp,
                'categorical_logit': None,
            }
        }
        
        if output_for == 'both':
            return output_transform['categorical_logit'][output_mode](
                tf.concat(categorical_logit, axis=-1)
            )
        else:
            logit = categorical_logit[output_for] \
                            - categorical_logit[1-output_for]
            return output_transform['logit'][output_mode](logit)