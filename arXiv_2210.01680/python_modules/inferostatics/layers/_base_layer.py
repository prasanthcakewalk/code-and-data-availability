from tensorflow.keras.layers import Layer

class _ScoreLLRBase(Layer):
    def __init__(self, potential_fn, expand_fn_args=False):
        super().__init__()
        
        # To avoid clashes with internal attribute and method names #######
        tmp = dir(self)
        for name in ['potential_fn', 'expand_fn_args',
                     'potential_callable']:
            assert name not in tmp
        ###################################################################
        
        self.potential_fn = potential_fn
        self.expand_fn_args = expand_fn_args
        if expand_fn_args:
            self.potential_callable = lambda l: self.potential_fn(*l)
        else:
            self.potential_callable = self.potential_fn