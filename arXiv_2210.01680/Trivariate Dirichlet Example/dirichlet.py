__all__ = [
    'param_shape', 'x_shape',
    'kernel_scale',
    'param_gen', 'x_gen', 'true_score_func'
]

import numpy as np
import scipy.special
import scipy.stats

param_shape = (3,)
x_shape = (3,)

param_range = ((.5, 5.), (.5, 5.), (.5, 5.))
kse_kernel_scale = (.25, .25, .25)
klre_kernel_scale = (.4, .4, .4)

def param_gen(N, rng):
    low = np.array(param_range)[:,0]
    high = np.array(param_range)[:,1]
    size = (N,) + param_shape
    return rng.uniform(low, high, size)

def x_gen(param, rng):
    output = np.array(param, copy=True)
    for i in range(len(param)):
        output[i] = rng.dirichlet(param[i])
    
    return output

def true_score_func(x, param):
    return (
        np.log(x)
        - scipy.special.digamma(param)
        + scipy.special.digamma(np.sum(param, axis=1, keepdims=True))
    )

def true_llr_func(x, param_0, param_1):
    answer = []
    for i in range(len(x)):
        answer.append(
            scipy.stats.dirichlet.logpdf(x[i], param_0[i])
            - scipy.stats.dirichlet.logpdf(x[i], param_1[i])
        )
    return np.array(answer)